<?php

/**
 * @file
 * Row style plugin for displaying the results as entities.
 */

/**
 * Plugin class for displaying Views results with entity_view.
 */
class search_api_solr_date_sort_views_plugin_row_entity_view extends entity_views_plugin_row_entity_view {

  protected $entity_type, $entities;
  public function option_definition() {
    $options = parent::option_definition();
    $options['date_field'] = array('default' => '_none');
    return $options;
  }

  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    // Add entity properties
    $entity_properties = entity_get_property_info($this->entity_type);
    // This is going to get nasty.
    $options = array();
    foreach ($entity_properties['bundles'] as $node_type) {
      foreach($node_type['properties'] as $name => $property) {
        dpm($property);
        if ($property['type'] === 'list<date>') {
          $options[$name] = $name;
        }
      }
    }
    // Add date fields.
    $date_fields = field_read_fields(array('module' => 'date'));
    if (!empty($date_fields)) {
      foreach ($date_fields as $name => $date_field) {
        $options[$name] = $name;
      }
    }

    $form['date_field'] = array(
      '#type' => 'select',
      '#title' => t('Date Field Instance'),
      '#options' => $options,
      '#default_value' => $this->options['date_field'],
    );

    return $form;
  }

  public function render($values) {
    $id = $values->entity;
    $dash_pos = strpos($id, "-");
    $eid = !empty($dash_pos) ? substr($id, 0, $dash_pos) : $id;
    $date_id = !empty($dash_pos) ? substr($id, $dash_pos + 1) : 0;
    if ($entity = $this->get_value($values)) {
      $render = $this->rendered_content[entity_id($this->entity_type, $entity)];
      $wrapper = entity_metadata_wrapper($this->entity_type, $entity);
      foreach($wrapper->{$this->options['date_field']}->value() as $key => $value) {
        if ($key != $date_id && is_numeric($key)) {
          unset($render[$this->options['date_field']][$key]);
        }
      }
      return drupal_render($render);
    }
  }
}
