<?php
/**
 * @file
 * Contains SearchApiViewsEventsSolrService.
 */

/**
 * Extension class with generic implementation of most service methods.
 */
class SearchApiSolrDateSortSolrService extends SearchApiSolrService {

  /**
   * {@inheritdoc}
   */
  public function query(SearchApiIndex $index, $options = array()) {
    // To Override the sort, we need to instantiate a different method.
    return new SearchApiSolrDateSortQuery($index, $options);
  }

  /**
   * {@inheritdoc}
   *
   * This alteration adds the first value of a multiple value date field as a string.
   * It then duplicates the events that have multiple dates, and inserts them as
   * a new document for each start date into the solr db.
   */
  protected function alterSolrDocuments(array &$documents, SearchApiIndex $index, array $items) {
    parent::alterSolrDocuments($documents, $index, $items);
    // Grab only the start date fields. Duplication isn't needed for end dates.
    $date_fields = _search_api_solr_date_sort_date_fields($index, 'dm');
    // Loop through each document to check for the date fields.
    foreach ($documents as $document) {
      // Loop Through each date field.
      //
      // @todo: Might want to limit this to a single field.
      foreach ($date_fields as $field) {
        // Grab all the date field values.
        $start_dates = $document->getField($field);
        $end_dates = $document->getField($field . '2');
        // If the start is not empty append the field.
        if (!empty($start_dates['value'])) {
          $string_field = substr_replace($field, 'ds_', 0, 3);
          // We account for multiple values of dates by storing each entity as a
          // new instance.
          $batches = array_chunk($start_dates['value'], 25, TRUE);
          foreach ($batches as $batch) {
            // Create a new array of new documents that will get added into the solr index.
            $new_documents = array();
            foreach ($batch as $i => $date) {
              if ($i == 0) {
                // For the first one, set the first start date.
                $document->setField($string_field, $date);
                // Set the end date.
                $document->setField($string_field . '2', $end_dates['value'][$i]);
              }
              else {
                // Clone the document to add it to the index.
                $new_document = clone $document;
                // If there are multiple dates, we need an id for each date. Thus,
                // append -###.
                $item_id = $document->getField('item_id');
                $item_id = $item_id['value'] . '-' . $i;
                // Set the new id as well
                $new_document->setField('item_id', $item_id);
                unset($new_document->$field);
                $field_2 = $field . '2';
                unset($new_document->$field_2);
                $hash = $document->getField('hash');
                $index_id = $document->getField('index_id');
                // Create a new id.
                $new_document->setField('id', $hash['value'] . '_' . $index_id['value'] . '_' . $item_id);
                // Add the specific date for this document.
                $new_document->setField($string_field, $date);
                // Set the end date.
                $new_document->setField($string_field . '2', $end_dates['value'][$i]);
                $new_documents[] = $new_document;
              }
            }
            $this->SearchApiSolrDateSortAddNewDocs($new_documents);
          }
        }
      }
    }
  }

  /**
   * Custom function to add the new documents.
   *
   * @param array $new_documents
   *   Array of duplcicated documents
   */
  protected function SearchApiSolrDateSortAddNewDocs($new_documents) {
    $ret = array();
    if (!empty($new_documents)) {
      try {
        $this->connect();
        $this->solr->addDocuments($new_documents);
        if (!empty($index->options['index_directly'])) {
          $this->scheduleCommit();
        }
        return $ret;
      }
      catch (SearchApiException $e) {
        watchdog_exception('search_api_solr', $e, "%type while indexing: !message in %function (line %line of %file).");
      }
    }
  }
}

