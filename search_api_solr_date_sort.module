<?php
/**
 * @file
 * File for search_api_solr_date_sort.
 */

/**
 * Implements hook_views_api().
 */
function search_api_solr_date_sort_views_api() {
  return array(
    'api' => 3,
  );
}

/**
 * Implementation of hook_views_data_alter().
 *
 * Add sorting by date.
 */
function search_api_solr_date_sort_views_data_alter(&$data) {
  foreach (search_api_index_load_multiple(FALSE) as $index) {
    $key = 'search_api_index_' . $index->machine_name;
    // Change the base query class to allow for sorting by date. This is needed
    // because we change the item_id to [id]-### for each additional date in a field.
    $data[$key]['table']['base']['query class'] = 'search_api_solr_date_sort_views_query';
    // Add the documented date string to the filter. They are not there originally,
    // because the field is a list.
    foreach($data[$key] as $k => $field) {
      if (isset($field['type']) && $field['type'] == 'list<date>') {
        // We copy over the base field, but adjust it to allow for the date to be
        // interpretted as the string.
        if (empty($field['real field'])) {
          $field['real field'] = $k;
        }

        $data[$key][$k . '_string'] = $field;
        $data[$key][$k . '_string']['title'] = t('Indexed String: ') . $field['title'];
        $data[$key][$k . '_string']['help'] = t('Single String version of ' . $field['real field']);
        $data[$key][$k . '_string']['type'] = 'date';
        // Default does not have a sort option, so we add it.
        $data[$key][$k . '_string']['sort'] = array(
          'handler' => 'SearchApiViewsHandlerSort',
        );
        if (isset($field['field'])) {
          $data[$key][$k . '_string']['field']['click sortable'] = TRUE;
        }
        // This is the field that is passed to the getFields() method and used for
        // the actual querying.
        $data[$key][$k . '_string']['real field'] = search_api_solr_date_sort_to_string($field['real field']);
      }
    }
  }
}

/**
 * Creates the initial callback fields
 */
function _search_api_solr_date_sort_date_fields($index, $prefix) {
  $fields = $index->getFields();
  $date_fields = array();
  foreach($fields as $id => $props) {
    if ($props['type'] == 'list<date>') {
      $name = search_api_solr_date_sort_to_string($id, $prefix);
      if (substr($id, -5) == 'value') {
        $date_fields[] = $name;
      }
    }
    else {
      continue;
    }
  }
  return $date_fields;
}

/**
 * Custom function to produce the proper name out of the real field id.
 */
function search_api_solr_date_sort_to_string($field, $prefix = 'ds') {
  return $prefix . '_' . str_replace(':', '$', $field);
}

/**
 * Implements hook_search_api_solr_field_mapping_alter(_.
 */
function search_api_solr_date_sort_search_api_solr_field_mapping_alter(SearchApiIndex $index, array &$fields) {
  $index_fields = $index->getFields();
  $date_fields = array();
  foreach($index_fields as $id => $props) {
    if ($props['type'] == 'list<date>') {
      $fields[search_api_solr_date_sort_to_string($id)] = search_api_solr_date_sort_to_string($id);
    }
  }
}

/**
 * Implements hook_search_api_service_info_alter().
 */
function search_api_solr_date_sort_search_api_service_info_alter(array &$service_info) {
  foreach ($service_info as $id => $info) {
    if ($id == 'search_api_solr_service') {
      // We need to extend the base class to allow fo ours to sort the
      $service_info[$id]['class'] = 'SearchApiSolrDateSortSolrService';
    }
  }
}

/**
 * Implements hook_search_api_solr_query_alter().
 */
function search_api_solr_date_sort_search_api_solr_query_alter(array &$call_args, SearchApiQueryInterface $query) {
  foreach ($call_args['params']['fq'] as $k => $filter) {
    if(substr($filter, 0, 2) == 'dm') {
      // Change the multi-value field to a string.
      $call_args['params']['fq'][$k] = str_replace('dm_', 'ds_', $filter);
    }
  }
}
